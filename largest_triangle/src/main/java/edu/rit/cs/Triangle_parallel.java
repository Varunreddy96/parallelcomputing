package edu.rit.cs;

import java.util.ArrayList;
import java.util.List;

import mpi.MPI;
import mpi.*;
import mpi.MPIException;

public class Triangle_parallel {
    private double maxarea = 0.0;
    private Point p1, p2, p3;
    private int index1, index2, index3;

    public double calc_area(double a, double b, double c) {
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        return area;
    }

    public double sidelen(Point p1, Point p2) {
        double xlen = Math.pow((p2.getX() - p1.getX()), 2);
        double ylen = Math.pow((p2.getY() - p1.getY()), 2);
        double len = Math.sqrt(xlen + ylen);
        return len;
    }

    public void findmax_triangle(List<Point> points, int start, int end) {
        for (int i = start; i < end; i++) {
            Point p1 = points.get(i);
            for (int j = i + 1; j < points.size() - 1; j++) {
                Point p2 = points.get(j);
                double a = sidelen(p1, p2);
                for (int k = j + 1; k < points.size(); k++) {
                    Point p3 = points.get(k);
                    double b = sidelen(p2, p3);
                    double c = sidelen(p1, p3);
                    if (!(a > (b + c) || b > (a + c) || c > (a + b))) {
                        double area = calc_area(a, b, c);
                        if (area > this.maxarea) {
                            this.maxarea = area;
                            this.p1 = p1;
                            this.p2 = p2;
                            this.p3 = p3;
                            this.index1 = i + 1;
                            this.index2 = j + 1;
                            this.index3 = k + 1;
                        }
                    }

                }
            }
        }
    }

    public void find_large_triangle(List<Point> points) throws MPIException {
        long start = System.currentTimeMillis();
        int rank = MPI.COMM_WORLD.getRank(), size = MPI.COMM_WORLD.getSize();
        int chunksize = points.size() / size;
        int sendbuffstart[] = new int[size];
        int sendbuffstop[] = new int[size];
        int rcvstart[] = new int[1];
        int rcvstop[] = new int[1];

        int temp = 0;
        for (int m = 0; m < size - 1; m++) {
            sendbuffstart[m] = temp;
            sendbuffstop[m] = temp + chunksize;
            temp = temp + chunksize;

        }
        sendbuffstart[size - 1] = temp;
        sendbuffstop[size - 1] = points.size();

        MPI.COMM_WORLD.scatter(sendbuffstart, 1, MPI.INT, rcvstart, 1, MPI.INT, 0);
        MPI.COMM_WORLD.scatter(sendbuffstop, 1, MPI.INT, rcvstop, 1, MPI.INT, 0);
//        System.out.println("Received rcvstart:" + rcvstart[0] + " rank : " + rank);
//        System.out.println("Received rcvstop: " + rcvstop[0] + " rank : " + rank);

        findmax_triangle(points, rcvstart[0], rcvstop[0]);


        double sBuf[] = {this.maxarea};
        double rBuf[] = new double[1];
        MPI.COMM_WORLD.allReduce(sBuf, rBuf, 1, MPI.DOUBLE, MPI.MAX);
        long end = System.currentTimeMillis();
        

        if (this.maxarea == rBuf[0]) {
//            System.out.println("maxarea: " + rBuf[0]);
//            System.out.println("index1: " + rBuf[1]+"--"+points.get((int)rBuf[1]).getX()+"-----"+points.get((int)rBuf[1]).getY());
//            System.out.println("index2: " + rBuf[2]+"--"+points.get((int)rBuf[2]).getX()+"-----"+points.get((int)rBuf[2]).getY());
//            System.out.println("index3: " + rBuf[3]+"--"+points.get((int)rBuf[3]).getX()+"-----"+points.get((int)rBuf[3]).getY());

            System.out.printf("%d %.5g %.5g%n", this.index1, points.get(this.index1-1).getX(), points.get(this.index1-1).getY());
            System.out.printf("%d %.5g %.5g%n", this.index2, points.get(this.index2-1).getX(), points.get(this.index2-1).getY());
            System.out.printf("%d %.5g %.5g%n", this.index3, points.get(this.index3-1).getX(), points.get(this.index3-1).getY());
            System.out.printf("%.5g%n", rBuf[0]);
            System.out.println("Time taken: "+(end-start));
        }


    }

    public static void main(String[] args) throws MPIException {
        
        MPI.Init(args);
        int numPoints = 100;
        Triangle_parallel max = new Triangle_parallel();
        RandomPoints rndPoints = new RandomPoints(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        List<Point> points = new ArrayList<>();
        int idx = 1;
        Point p;
        while (rndPoints.hasNext()) {
            p = rndPoints.next();
            points.add(p);
            //System.out.println(idx+": x=" + p.getX() + " y=" + p.getY());
            //idx++;
        }

        max.find_large_triangle(points);

//        System.out.printf("%d %.5g %.5g%n", max.index1, max.p1.getX(), max.p1.getY());
//        System.out.printf("%d %.5g %.5g%n", max.index2, max.p2.getX(), max.p2.getY());
//        System.out.printf("%d %.5g %.5g%n", max.index3, max.p3.getX(), max.p3.getY());
//        System.out.printf("%.5g%n", max.maxarea);
        MPI.Finalize();
        

    }
}
